const http = require("http");

const port = 3000;

const server = http.createServer(function (request, response) {

	if(request.url == "/login"){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end("Welcome to the Log-in Page!");

	} else if(request.url == "/portfolio") {
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end("https://jayzher.github.io/Web-Portfolio")
	} else {
		response.writeHead(404, {'Content-Type': 'text/plain'});
		response.end("I'm Sorry the Page you are Looking for Cannot be Found!")
	}
})

server.listen(port);

console.log(`Server is now accessible at localhost:${port}`);
